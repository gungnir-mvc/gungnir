<?php
declare(strict_types=1);
error_reporting(E_ALL);
ini_set('display_errors', '1');
define('EXT', '.php');
define('ROOT', dirname(dirname(__FILE__)) . '/');
define('APPROOT', ROOT . 'application/');
define('SYSROOT', ROOT . 'system/');
define('VENROOT', ROOT . 'vendor/');
define('BASE_DIR', 'gungnir');
